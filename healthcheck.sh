#!/bin/sh

OPENVPN=$(pgrep openvpn)
RTORRENT=$(pgrep rtorrent)

if [ -z "${OPENVPN}" ] && [ -z "${RTORRENT}" ]; then
    echo "ERROR: Healthcheck failed, either openvpn or rtorrent is stopped"
    exit 1
fi

if [ -z "${HEALTHCHECK_HOST}" ]; then
    HEALTHCHECK_HOST="iana.org"
fi


