ARG BASE=alpine:3.11

FROM $BASE as build
# ENV VERSION=$VERSION
# ENV LIBTO_VERSION=$LIBTO_VERSION
ARG VERSION=0.9.8
ARG LIBTO_VERSION=0.13.8

RUN apk add --no-cache git curl autoconf automake make alpine-sdk libtool ncurses-dev zlib-dev libsigc++-dev openssl-dev linux-headers curl-dev xmlrpc-c-dev ncurses-dev
RUN mkdir /newroot

RUN git clone --branch v${LIBTO_VERSION} --single-branch https://github.com/rakshasa/libtorrent.git /libtorrent
WORKDIR /libtorrent
RUN ./autogen.sh && \
    ./configure \
        --prefix=/usr \
        --disable-debug \
        --disable-instrumentation && \
    make -j$(nproc) && \
    DESTDIR=/newroot make install && \
    make install

RUN git clone --branch v${VERSION} --single-branch https://github.com/rakshasa/rtorrent.git /work
WORKDIR /work

RUN ./autogen.sh && \
    ./configure \
        --prefix=/usr \
        --disable-debug \
        --with-xmlrpc-c && \
    make -j$(nproc) && \
    DESTDIR=/newroot make install

FROM $BASE

LABEL maintainer="Max Audron <audron@cocaine.farm>"
LABEL version=$VERSION
LABEL description="A Docker image based on alpine for rtorrent with optional openvpn, able to run as an unprivileged container."

RUN apk add --no-cache nftables ip6tables curl libgcc libstdc++ musl ncurses-libs xmlrpc-c openvpn wireguard-tools jq

RUN rm /sbin/iptables /sbin/ip6tables
RUN ln /sbin/ip6tables-nft /sbin/ip6tables -s
RUN ln /sbin/iptables-nft /sbin/iptables -s

COPY --from=build /newroot/usr/bin/rtorrent /usr/bin/rtorrent
COPY --from=build /newroot/usr/lib/libtorrent.so* /usr/lib

ENV RT_BASE_DIR="/data/" \
    RT_TRACKER_UDP="yes" \
    RT_DHT_MODE="disable" \
    RT_DHT_PORT=49160 \
    RT_PROTO_PEX="no" \
    RT_MAX_UP=100 \
    RT_MAX_UP_GLOBAL=250 \
    RT_MIN_PEERS=20 \
    RT_MAX_PEERS=60 \
    RT_MIN_PEERS_SEED=30 \
    RT_MAX_PEERS_SEED=80 \
    RT_TRACKERS_WANT=80 \
    RT_MEMORY_MAX="1800M" \
    RT_DAEMON="true" \
    RT_XMLRPC_BIND="0.0.0.0" \
    RT_XMLRPC_PORT="5000" \
    RT_CONFIG_FILE="/.rtorrent.rc" \
    CREATE_TUN_DEVICE="" \
    OPENVPN_CONFIG_URL="" \
    OPENVPN_CONFIG_PATH="" \
    OPENVPN_USER="" \
    OPENVPN_PASS="" \
    OPENVPN_AUTH_FILE="" \
    LOCAL_NETWORK="" \
    WIREGUARD_CONFIG_PATH="" \
    VPN_TYPE="openvpn"

RUN adduser -u 911 -D abc && \
    addgroup abc ping && \
    mkdir /data && chown abc /data && \
    mkdir /private && chown abc /private

COPY rtorrent/.rtorrent.rc /.rtorrent.rc
COPY entrypoint.sh /entrypoint.sh

VOLUME ["/data"]

# USER abc
CMD ["/entrypoint.sh"]
